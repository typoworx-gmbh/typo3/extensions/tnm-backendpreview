# TYPO3 Extension: (tnm) BackendPreview

## Purpose of this extension
This extension can be used to create custom Backend-Preview's for Content-Elements
using a Fluid-Section named 'BackendPreview' in the regular Fluid Frontend-Template. 
Handling is similar to how Flux handles this.

DateProcessors are respected and also Processed for Backend-Preview if the Content-Element (Typoscript Processor-Definitions)
are used.

**Example Fluid-Template:**
```html
<html
    data-namespace-typo3-fluid="true"
    xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers"
    xmlns:tnmp="http://typo3.org/ns/TYPOworx/BackendPreview/ViewHelpers"
>
    <!-- Backend Preview -->
    <f:section name="BackendPreview">
        <f:comment><!-- Example for different Layouts/Previews --></f:comment>
        <f:switch expression="{data.layout}">
            <f:case value="100">
                <table class="table table-sm align-top">
                    <tbody>
                    <tr>
                        <th scope="row" class="col-sm-1">Layout</th>
                        <td>Teaser</td>
                    </tr>
                    </tbody>
                </table>
                <f:comment><!-- Example use Bootstrap5 Cards or Data-Table here --></f:comment>
                <div class="card-container">
                    <f:for each="{data_tx_myteaser_teaser}" as="data_item">
                        <div class="card card-size-fixed-small pt-0 pb-0">
                            <div class="card-header">
                                <div class="card-img-top">
                                    <f:alias map="{image: data_item.data_tx_myteaser_image.0}">
                                        <f:if condition="{image}">
                                            <f:image image="{image}" cropVariant="teaser-2023" class="fluid" alt="{image.alternative}" title="{image.title}" height="150" />
                                        </f:if>
                                    </f:alias>
                                </div>
                            </div>
                            <div class="card-body">
                                <strong class="card-title">{data_item.data.tx_myteaser_header -> f:format.nl2br()}</strong>
                                <f:comment><!-- This Datatable contains properties we want to show in preview --></f:comment>
                                <table class="table table--datatable table-sm align-top">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Link</th>
                                        <td><tnmp:backend.link.editRecordLink link="{data_item.data.tx_myteaser_link}" /></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </f:for>
                </div>
            </f:case>
        </f:switch>
    </f:section>
</html>
```
