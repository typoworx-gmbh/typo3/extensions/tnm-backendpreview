<?php
declare(strict_types=1);
namespace TYPOworx\BackendPreview\ViewHelpers\Backend\Link;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Http\ServerRequestFactory;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPOworx\BackendPreview\Backend\LinkHandling\ParseLink;

class EditRecordLinkViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;


    public function initializeArguments(): void
    {
        $this->registerArgument('link', 'string', 't3:// Link for the table for the record icon', true);
        $this->registerArgument('size', 'string', 'the icon size', false, Icon::SIZE_SMALL);
        $this->registerArgument('alternativeMarkupIdentifier', 'string', 'alternative markup identifier', false);
    }

    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) :? string
    {
        $t3link = $arguments['link'] ?? null;

        if (empty($t3link))
        {
            return null;
        }

        $linkParts = ParseLink::toArray($t3link);
        $returnUrl = ServerRequestFactory::fromGlobals()->getUri();

        if (empty($linkParts['table']) || empty($linkParts['uid']))
        {
            return null;
        }

        $subRenderingContext = new RenderingContext(
            $renderingContext->getViewHelperResolver(),
            $renderingContext->getCache(),
            $renderingContext->getTemplateProcessors(),
            $renderingContext->getExpressionNodeTypes(),
            $renderingContext->getTemplatePaths()
        );

        $recordRow = BackendUtility::getRecord($linkParts['table'], $linkParts['uid']);
        $recordTitle = BackendUtility::getRecordTitle($linkParts['table'], $recordRow);

        $template = $subRenderingContext->getTemplateParser()->parse(trim('
            {namespace be=TYPO3\CMS\Backend\ViewHelpers}
            {namespace core=TYPO3\CMS\Core\ViewHelpers}
            <be:link.editRecord uid="{uid}" table="{table}" returnUrl="{returnUrl}">
                <core:iconForRecord table="{table}" row="{uid: uid}" size="{arguments.size}" alternativeMarkupIdentifier="{arguments.alternativeMarkupIdentifier}"/> {recordTitle}
            </be:link.editRecord>
        '));

        $variableContainer = $subRenderingContext->getVariableProvider();
        $variableContainer->setSource([
            'arguments' => $arguments,
            'recordTitle' => $recordTitle,
            'table' => $linkParts['table'],
            'uid' => $linkParts['uid'],
            'returnUrl' => (string)$returnUrl,
        ]);

        return $template->render($subRenderingContext);
    }
}
