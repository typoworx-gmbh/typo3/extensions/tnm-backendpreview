<?php
declare(strict_types=1);
namespace TYPOworx\BackendPreview\Backend\Preview;

use TYPO3\CMS\Backend\View\PageLayoutContext;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\DataProcessing\DataProcessorRegistry;
use TYPO3Fluid\Fluid\View\Exception\InvalidSectionException;
use TYPOworx\BackendPreview\Utility\Backend\TypoScriptFrontentFactory;

class BackendSectionRenderer
{
    private ?PageLayoutContext $pageLayoutContext = null;


    public function __construct(
        private readonly BackendConfigurationManager $backendConfigurationManager,
        private readonly DataProcessorRegistry $dataProcessorRegistry,
        private TypoScriptFrontentFactory $typoScriptFrontentFactory
    )
    {}

    public function setPageLayoutContext(PageLayoutContext $pageLayoutContext)
    {
        $this->pageLayoutContext = $pageLayoutContext;
    }

    protected function validateFluidConfiguragtion(?array $fluidConfiguration) : bool
    {
        if (!is_array($fluidConfiguration) || empty($fluidConfiguration))
        {
            return false;
        }

        if (isset($fluidConfiguration['templateName']) && empty($fluidConfiguration['templateName']))
        {
            return false;
        }

        if (empty($fluidConfiguration['layoutRootPaths']))
        {
            return false;
        }

        if (empty($fluidConfiguration['partialRootPaths']))
        {
            return false;
        }

        if (empty($fluidConfiguration['templateRootPaths']))
        {
            return false;
        }

        return true;
    }

    public function render(array $row): ?string
    {
        // Minimal bootstrap to have Plugin-Configuration
        $cType = $row['CType'] ?? null;
        $fluidConfiguration = $this->resolveViewConfiguration($cType);

        if (!$fluidConfiguration || !$this->validateFluidConfiguragtion($fluidConfiguration))
        {
            return null;
        }

        $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);

        $standaloneView->setTemplate($fluidConfiguration['templateName']);
        $standaloneView->setLayoutRootPaths($fluidConfiguration['layoutRootPaths']);
        $standaloneView->setPartialRootPaths($fluidConfiguration['partialRootPaths']);
        $standaloneView->setTemplateRootPaths($fluidConfiguration['templateRootPaths']);

        $variables = [];
        $variables['data'] = $row;
        $variables = $this->applyDataProcessors($row, $variables);

        $content = null;
        foreach(['backendPreview', 'BackendPreview'] as $sectionName)
        {
            try
            {
                $content = $standaloneView->renderSection(sectionName: $sectionName, variables: $variables, ignoreUnknown: true);
            }
            catch (InvalidSectionException $e)
            {}
        }

        return empty($content) ? null : $content;
    }

    public function getTyposcriptConfiguration(bool $plainArray = true) : array
    {
        $configuration = $this->backendConfigurationManager->getTypoScriptSetup();

        if (!$plainArray)
        {
            return $configuration;
        }

        return GeneralUtility::makeInstance(TypoScriptService::class)->convertTypoScriptArrayToPlainArray($configuration);
    }

    public function applyDataProcessors(array $row, array $processedData = []) :? array
    {
        $cType = $row['CType'] ?? null;
        if (empty($cType))
        {
            return null;
        }

        $typoscriptConfiguration = $this->getTyposcriptConfiguration(false);
        $dataProcessors = $typoscriptConfiguration['tt_content.'][ $cType . '.' ]['dataProcessing.'] ?? [];

        $contentObjectRenderer = $this->getPreparedContentObjectRenderer($this->pageLayoutContext->getPageId(), $row, 'tt_content');

        foreach($dataProcessors as $index => $identifier)
        {
            if (!is_int($index))
            {
                continue;
            }

            $dataProcessor = $this->dataProcessorRegistry->getDataProcessor($identifier)
                 ?? GeneralUtility::makeInstance($identifier)
            ;

            if ($dataProcessor instanceof DataProcessorInterface)
            {
                $processedData = $dataProcessor->process(
                    cObj: $contentObjectRenderer,
                    contentObjectConfiguration: $typoscriptConfiguration,
                    processorConfiguration: $dataProcessors[ $index . '.' ] ?? [],
                    processedData: $processedData
                );
            }
        }

        return $processedData;
    }

    public function getPreparedContentObjectRenderer(int $pageId, array $row, ?string $tableName = null)
    {
        $contentObjectRenderer = GeneralUtility::makeInstance(
            ContentObjectRenderer::class,
            $this->typoScriptFrontentFactory->factory($pageId)
        );

        if (!empty($row))
        {
            $contentObjectRenderer->start($row, $tableName ?? null);
        }

        return $contentObjectRenderer;
    }

    public function resolveViewConfiguration(string $cType) :? array
    {
        if (empty($cType))
        {
            return null;
        }

        if (!str_contains($cType, '_'))
        {
            // looks like core CE
            return null;
        }

        [$extensionName, $pluginName] = explode('_', $cType, 2);

        $tsExtensionName = ExtensionManagementUtility::getCN($extensionName);

        $configuration = $this->getTyposcriptConfiguration();
        $templateName = $this->getArrayPath(
            $configuration,
            sprintf('tt_content.%s.templateName', $cType),
            ucfirst($pluginName)
        );

        $layoutRootPaths =  $this->getArrayPath(
            $configuration,
            sprintf('plugin.%s.view.layoutRootPaths', $tsExtensionName),
            sprintf('EXT:%s', $extensionName)
        );
        $partialRootPaths =  $this->getArrayPath(
            $configuration,
            sprintf('plugin.%s.view.partialRootPaths', $tsExtensionName)
        );
        $templateRootPaths =  $this->getArrayPath(
            $configuration,
            sprintf('plugin.%s.view.templateRootPaths', $tsExtensionName)
        );

        return [
            'templateName' => $templateName,
            'layoutRootPaths' => $layoutRootPaths,
            'partialRootPaths' => $partialRootPaths,
            'templateRootPaths' => $templateRootPaths,
        ];
    }

    protected function getArrayPath(array $array, string $path, mixed $default = null) : mixed
    {
        try
        {
            return ArrayUtility::getValueByPath($array, $path, '.');
        }
        catch (MissingArrayPathException $e)
        {}

        return $default;
    }
}
