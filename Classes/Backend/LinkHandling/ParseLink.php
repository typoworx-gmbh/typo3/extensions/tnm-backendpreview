<?php
declare(strict_types=1);
namespace TYPOworx\BackendPreview\Backend\LinkHandling;

use TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ParseLink
{
    public static function toArray(?string $t3link): ?array
    {
        $return = [];

        if (empty($t3link))
        {
            return null;
        }

        try
        {
            $linkService = GeneralUtility::makeInstance(LinkService::class);
            $linkData = $linkService->resolve($t3link);

            switch ($linkData['type'])
            {
                case LinkService::TYPE_PAGE:
                    $return['uid'] = (int)($linkData['pageuid'] ?? 0);
                    $return['table'] = 'pages';

                    if (isset($linkData['pagetype']))
                    {
                        $return['pagetype'] = (int)$linkData['pagetype'];
                    }

                    if (isset($linkData['fragment']))
                    {
                        $return['fragment'] = $linkData['fragment'];
                    }
                    break;

                case LinkService::TYPE_FILE:
                case LinkService::TYPE_UNKNOWN:
                    if (isset($linkData['file']))
                    {
                        $return['type'] = LinkService::TYPE_FILE;
                        $return['table'] = 'sys_file_reference';
                        $return['file'] = $linkData['file'] instanceof FileInterface ? $linkData['file']->getUid() : $linkData['file'];
                    }
                    else
                    {
                        $pU = parse_url($t3link);
                        parse_str($pU['query'] ?? '', $query);

                        if (isset($query['uid']))
                        {
                            $return['type'] = LinkService::TYPE_FILE;
                            $return['table'] = 'sys_file';
                            $return['file'] = (int)$query['uid'];
                        }
                    }
                    break;

                case LinkService::TYPE_EMAIL:
                    $pU = parse_url($t3link);
                    parse_str($pU['query'] ?? '', $return);

                    $return['email'] = $pU['path'];
                    break;

                case LinkService::TYPE_TELEPHONE:
                    $pU = parse_url($t3link);

                    $return['phone'] = $pU['path'];
                    break;
            }
        }
        catch (UnknownLinkHandlerException $e)
        {}

        if ($return !== [])
        {
            $return['type'] = $return['type'] ?? null;
            $return['linktype'] = $linkData['type'];

            return $return;
        }

        return null;
    }
}
