<?php
declare(strict_types=1);
namespace TYPOworx\BackendPreview\Utility\Backend;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Site\Entity\SiteInterface;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class TypoScriptFrontentFactory implements SingletonInterface
{
    public function __construct(
        private readonly Context $context,
        private readonly FrontendUserAuthentication $frontendUser
    )
    {}

    public function factory(int $pageId = 0) :? TypoScriptFrontendController
    {
        /** @var TypoScriptFrontendController $tsfeController */
        $tsfeController = GeneralUtility::makeInstance(
            TypoScriptFrontendController::class,
            $this->context,
            $this->getSite(),
            $this->getSiteLanguage(),
            $this->createPageArguments($pageId),
            $this->frontendUser
        );

        $tsfeController->determineId($this->getServerReqest());

        return $tsfeController;
    }

    protected function getServerReqest() :? ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }

    protected function getSite() :? SiteInterface
    {
        return $this->getServerReqest()?->getAttribute('site');
    }

    protected function getSiteLanguage() :? SiteLanguage
    {
        $siteLanguages = $this->getSite()?->getLanguages();

        if (!empty($siteLanguages))
        {
            foreach ($siteLanguages as $language)
            {
                //@ToDo this should match the BE-Language or current TSFE-Language
                if ($language->isEnabled())
                {
                    return $language;
                }
            }
        }

        return null;
    }

    protected function createPageArguments(int $pageId, ?string $pageType = '0')
    {
        return new PageArguments($pageId, $pageType ?? '', []);
    }
}
