<?php
declare(strict_types=1);
namespace TYPOworx\BackendPreview\EventListener\Backend;

use TYPOworx\BackendPreview\Backend\Preview\BackendSectionRenderer;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;

class PageContentPreviewRendering
{
    private BackendSectionRenderer $backendSectionRenderer;

    public function __construct(BackendSectionRenderer $backendSectionRenderer)
    {
        $this->backendSectionRenderer = $backendSectionRenderer;
    }

    public function __invoke(PageContentPreviewRenderingEvent $event): void
    {
        $record = $event->getRecord();
        $event->setRecord($record);

        $this->backendSectionRenderer->setPageLayoutContext($event->getPageLayoutContext());
        $preview = $this->backendSectionRenderer->render($record);

        if (!empty($preview))
        {
            $event->setPreviewContent($preview);
        }
    }
}
