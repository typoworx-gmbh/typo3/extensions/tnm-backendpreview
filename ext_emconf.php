<?php
$EM_CONF[$_EXTKEY] = [
    'title' => '(tnm) BackendPreview',
    'description' => 'Provides Backend-Preview for custom Content-Elements by Fluid-Section \'backend-preview\'',
    'category' => 'backend',
    'author' => 'Gabriel Kaufmann',
    'author_email' => 'info@typoworx.com',
    'author_company' => 'TYPOworx GmbH',
    'state' => 'beta',
    'version' => '1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5-13.0'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'TYPOworx\\BackendPreview\\' => 'Classes'
        ],
    ],
];
